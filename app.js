var express = require('express');
var app = express();

app.options(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'twig');
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});

var server = app.listen(3050, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
  console.log(app);
});
